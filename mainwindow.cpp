#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{

    ui->setupUi(this);

    ui->spinBoxS->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinBoxSalt->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinTks->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinZagr->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinZagr_2->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinZagr_3->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinCargoA->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinCargoB->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinCargoC->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinCargoD->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinBg4->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinBg6->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinBSP->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->spinBSP1->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineLbs1->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineKg2->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineEditG_2->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineGP->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineMM->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineHA->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineQNH->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineQFE->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineMC->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineKM->setInputMethodHints(Qt::ImhDigitsOnly);
    //ui->lineMK->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineUV->setInputMethodHints(Qt::ImhDigitsOnly);
    ui->lineU->setInputMethodHints(Qt::ImhDigitsOnly);
    //ui->lineVet->setInputMethodHints(Qt::ImhDigitsOnly);
    //ui->cbLeto->set;
    //ui->cbLeto->setCurrentIndex(0);
    //ui->web->setResizeMode(QQuickWidget::SizeRootObjectToView);

    connect(ui->exit, SIGNAL(triggered()), this, SLOT(closeapp()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(closeapp()));

    connect(ui->aboutApp, SIGNAL(triggered()), this, SLOT(about()));
    connect(ui->helpApp, SIGNAL(triggered()), this, SLOT(help()));
    connect(ui->butRasch, SIGNAL(clicked()), this, SLOT(fuel()));
    connect(ui->butRaschVes, SIGNAL(clicked()), this, SLOT(ves()));
    connect(ui->comboBrake, SIGNAL(currentIndexChanged(int)), ui->comboWind, SLOT(setCurrentIndex(int)));
    connect(ui->comboWind, SIGNAL(currentIndexChanged(int)), ui->comboBrake, SLOT(setCurrentIndex(int)));
    connect(ui->cbBort, SIGNAL(currentIndexChanged(int)), this, SLOT(pola(int)));
    connect(ui->cbLeto, SIGNAL(currentIndexChanged(int)), this, SLOT(leto()));
    connect(ui->pbKg, SIGNAL(clicked()), this, SLOT(kg()));
    connect(ui->pbLbs, SIGNAL(clicked()), this, SLOT(lbs()));
    connect(ui->pbPress, SIGNAL(clicked()), this, SLOT(mm()));
    connect(ui->pbH, SIGNAL(clicked()), this, SLOT(ha()));
    connect(ui->pbU, SIGNAL(clicked()), this, SLOT(vet()));
    connect(ui->pbMC, SIGNAL(clicked()), this, SLOT(mc()));
    //connect(ui->tab_5, SIGNAL(changed()),this,SLOT(setHidden(bool)));
    connect(ui->pbZapr, SIGNAL(clicked()), this, SLOT(mettaf()));


    ui->spinBSP->setVisible(false);
    ui->spinBSP1->setVisible(false);
    ui->label_31->setVisible(false);
    ui->label_32->setVisible(false);
    ui->spinZagr->setMaximum(300);
    ui->spinZagr_2->setMaximum(300);
    ui->spinZagr_3->setMaximum(300);
     if (ui->cbLeto->currentIndex() == 0)
    {
        ui->spinZagr->setSingleStep(75);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setSingleStep(75);
        ui->spinZagr_2->setMaximum(300);
        ui->spinZagr_3->setMaximum(300);
    }
    else if (ui->cbLeto->currentIndex() == 1)
    {
        ui->spinZagr->setSingleStep(80);
        ui->spinZagr_2->setSingleStep(80);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(300);
        ui->spinZagr_3->setMaximum(300);
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::leto()
{

     if (ui->cbLeto->currentIndex() == 0)
    {
        ui->spinZagr->setSingleStep(75);
        ui->spinZagr_2->setSingleStep(75);

    }
    else if (ui->cbLeto->currentIndex() == 1)
    {
        ui->spinZagr->setSingleStep(80);
        ui->spinZagr_2->setSingleStep(80);
    }
}

void MainWindow::fuel()
{
    int v = ui->spinBoxV->value();
    int S = ui->spinBoxS->value();
    int Salt = ui->spinBoxSalt->value();
    float Tzap = (float)Salt/v;//Время полета до запасного
    float Tp = (float)S/v; //Время полета до ап
    int Hh = (int)Tp;
    int Mm = (int)((Tp- Hh)*60);
    QTime n;
    n.setHMS(Hh,Mm,00,00);
    int Gpol = (Tp*150*0.03)+(Tp*150);
    int Gzap = (Tzap*150)+75;
    int Gf = Gpol + Gzap;
    ui->lineEditGp->setText(QString::number(Gpol));//Вывод топлива на полет
    ui->lineEditGz->setText(QString::number(Gzap));//Вывод топлива до запасного
    ui->lineEditG->setText(QString::number(Gf));//Вывод общего топлива
    ui->lineEditG_2->setText(QString::number(Gf));//Вывод общего топлива для загрузки
    ui->timeEdit->setTime(n);//Вывод времени полёта
}

void MainWindow::pola(int comboIndex)
{

    //В зависимости от того какой элемент выбран в комбобоксе делаем видимыми или невидимыми
    //определенные спины и лейблы
    switch(comboIndex)
    {
    case 0://67422П

        leto();
        ui->spinBSP->setVisible(false);
        ui->spinBSP1->setVisible(false);
        ui->label_31->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(300);
        ui->spinZagr_3->setMaximum(300);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr_2->setMaximum(300);
            ui->spinZagr_3->setMaximum(300);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(300);
            ui->spinZagr_3->setMaximum(300);
        }
        break;
    case 1://67422C1
        leto();
        ui->spinBSP->setVisible(true);
        ui->label_31->setVisible(true);
        ui->spinBSP1->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(200);
        ui->spinZagr_3->setMaximum(100);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(200);
            ui->spinZagr_3->setMaximum(100);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(200);
            ui->spinZagr_3->setMaximum(100);
        }
        break;
    case 2://67422C2
        leto();
        ui->spinBSP1->setVisible(true);
        ui->label_32->setVisible(true);
        ui->spinBSP->setVisible(true);
        ui->label_31->setVisible(true);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(100);
        ui->spinZagr_3->setMaximum(100);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(100);
            ui->spinZagr_3->setMaximum(100);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(100);
            ui->spinZagr_3->setMaximum(100);
        }
        break;
    case 3://67720П
        leto();
        ui->spinBSP->setVisible(false);
        ui->spinBSP1->setVisible(false);
        ui->label_31->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(300);
        ui->spinZagr_3->setMaximum(300);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(300);
            ui->spinZagr_3->setMaximum(300);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(300);
            ui->spinZagr_3->setMaximum(300);
        }
        break;
    case 4://674720C1
        leto();
        ui->spinBSP->setVisible(true);
        ui->label_31->setVisible(true);
        ui->spinBSP1->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(200);
        ui->spinZagr_3->setMaximum(100);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(200);
            ui->spinZagr_3->setMaximum(100);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(200);
            ui->spinZagr_3->setMaximum(100);
        }
        break;
    case 5://674720C2
        leto();
        ui->spinBSP1->setVisible(true);
        ui->label_32->setVisible(true);
        ui->spinBSP->setVisible(true);
        ui->label_31->setVisible(true);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(100);
        ui->spinZagr_3->setMaximum(100);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(100);
            ui->spinZagr_3->setMaximum(100);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(100);
            ui->spinZagr_3->setMaximum(100);
        }
        break;
    case 6://674720ПЕР
        leto();
        ui->spinBSP1->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinBSP->setVisible(false);
        ui->label_31->setVisible(false);
        ui->spinZagr_2->setVisible(false);
        ui->label_8->setVisible(false);
        ui->spinZagr->setMaximum(160);
        ui->spinZagr_3->setMaximum(160);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr->setMaximum(160);
            ui->spinZagr_3->setMaximum(160);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr->setMaximum(160);
            ui->spinZagr_3->setMaximum(160);
        }
        break;
    case 7://674720РАБ
        leto();
        ui->spinBSP1->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinBSP->setVisible(false);
        ui->label_31->setVisible(false);
        ui->spinZagr_2->setVisible(false);
        ui->label_8->setVisible(false);
        ui->spinZagr->setMaximum(160);
        ui->spinZagr_3->setMaximum(160);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr->setMaximum(160);
            ui->spinZagr_3->setMaximum(160);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr->setMaximum(160);
            ui->spinZagr_3->setMaximum(160);
        }
        break;
    case 8://67450П
        leto();
        ui->spinBSP->setVisible(false);
        ui->spinBSP1->setVisible(false);
        ui->label_31->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(300);
        ui->spinZagr_3->setMaximum(300);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(300);
            ui->spinZagr_3->setMaximum(300);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(300);
            ui->spinZagr_3->setMaximum(300);
        }
        break;
    case 9://67450C1
        leto();
        ui->spinBSP->setVisible(true);
        ui->label_31->setVisible(true);
        ui->spinBSP1->setVisible(false);
        ui->label_32->setVisible(false);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(200);
        ui->spinZagr_3->setMaximum(100);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(200);
            ui->spinZagr_3->setMaximum(100);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(200);
            ui->spinZagr_3->setMaximum(100);
        }
        break;
    case 10://67450C2
        leto();
        ui->spinBSP1->setVisible(true);
        ui->label_32->setVisible(true);
        ui->spinBSP->setVisible(true);
        ui->label_31->setVisible(true);
        ui->spinZagr_2->setVisible(true);
        ui->label_8->setVisible(true);
        ui->spinZagr->setMaximum(300);
        ui->spinZagr_2->setMaximum(100);
        ui->spinZagr_3->setMaximum(100);
        if (ui->cbLeto->currentIndex() == 0)
        {
            ui->spinZagr->setSingleStep(75);
            ui->spinZagr_2->setSingleStep(75);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(100);
            ui->spinZagr_3->setMaximum(100);
        }
        else if (ui->cbLeto->currentIndex() == 1)
        {
            ui->spinZagr->setSingleStep(80);
            ui->spinZagr_2->setSingleStep(80);
            ui->spinZagr->setMaximum(300);
            ui->spinZagr_2->setMaximum(100);
            ui->spinZagr_3->setMaximum(100);
        }
        break;
    }
}
void MainWindow::ves()
{
    //67422_пасс
    if (ui->cbBort->currentIndex() == 0){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2393;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.1731)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+((float)ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        float CG = (((1018.27+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*265.8)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)

        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67422_сз1
    else if (ui->cbBort->currentIndex() == 1){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2367;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinBSP->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+((float)ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((1004.72+22.35+(((float)Gfuel/0.4536)/4.92)+23.9+23.9+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*265.8)/1000)+((((float)ui->spinBSP->value()/0.4536)*248.3)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67422_сз2
    else if (ui->cbBort->currentIndex() == 2){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2362;//вес пустого кг без кислорода
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinBSP->value())+((float)ui->spinBSP1->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((1001.96+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*295.0)/1000)+((((float)ui->spinBSP1->value()/0.4536)*207.3)/1000)+((((float)ui->spinBSP->value()/0.4536)*261.3)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }

    //67720_пасс
    else if (ui->cbBort->currentIndex() == 3){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2372;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.1731)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+((float)ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((1010.73+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*265.8)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67720_сз1
    else if (ui->cbBort->currentIndex() == 4){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2346;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinBSP->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+((float)ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((997.99+22.35+(((float)Gfuel/0.4536)/4.92)+23.9+23.9+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*265.8)/1000)+((((float)ui->spinBSP->value()/0.4536)*248.3)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67720_сз2
    else if (ui->cbBort->currentIndex() == 5){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2341;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinBSP->value())+((float)ui->spinBSP1->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((994.42+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*295.0)/1000)+((((float)ui->spinBSP1->value()/0.4536)*207.3)/1000)+((((float)ui->spinBSP->value()/0.4536)*261.3)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67720_перегон
    else if (ui->cbBort->currentIndex() == 6){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2403;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((1026.46+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*269.6)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67720_работа
    else if (ui->cbBort->currentIndex() == 7){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2403;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((1033.36+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*269.6)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67450_пасс
    if (ui->cbBort->currentIndex() == 8){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2403;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.1731)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+((float)ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        float CG = (((1020.97+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*265.8)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)

        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67450_сз1
    else if (ui->cbBort->currentIndex() == 9){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2377;//вес пустого кг
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinBSP->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+((float)ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((1008.23+22.35+(((float)Gfuel/0.4536)/4.92)+23.9+23.9+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*265.8)/1000)+((((float)ui->spinBSP->value()/0.4536)*248.3)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
    //67450_сз2
    else if (ui->cbBort->currentIndex() == 10){
        ui->lineVzl->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        ui->lineCentr->setStyleSheet("color: green; background-color: rgb(255, 255, 255);");
        float Gpust = 2371;//вес пустого кг без кислорода
        int Gsl = 39;
        int Gfuel = ui->lineEditG_2->text().toInt();
        int vEk = 80;
        float Gvzl = Gpust+(float)Gsl+(float)Gfuel+((float)ui->spinTks->value()*4.173)+((float)ui->spinEk->value()*vEk)+((float)ui->spinZagr->value())+((float)ui->spinZagr_2->value())+((float)ui->spinZagr_3->value())+((float)ui->spinBSP->value())+((float)ui->spinBSP1->value())+((float)ui->spinCargoA->value())+((float)ui->spinCargoB->value())+((float)ui->spinCargoC->value())+(ui->spinCargoD->value())+((float)ui->spinBg4->value())+((float)ui->spinBg6->value())-(float)16;
        ui->lineVzl->setText(QString::number(round(Gvzl)));
        if (Gvzl>3969) {
            ui->lineVzl->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Превышение веса!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }
        float CG = (((1004.66+22.35+(((float)Gfuel/0.4536)/4.92)+23.90+23.90+((((float)ui->spinZagr->value()/0.4536)*192.6)/1000)+((((float)ui->spinZagr_2->value()/0.4536)*229.2)/1000)+((((float)ui->spinZagr_3->value()/0.4536)*295.0)/1000)+((((float)ui->spinBSP1->value()/0.4536)*207.3)/1000)+((((float)ui->spinBSP->value()/0.4536)*261.3)/1000)+((((float)ui->spinCargoA->value()/0.4536)*132.4)/1000)+((((float)ui->spinCargoB->value()/0.4536)*182.1)/1000)+((((float)ui->spinCargoC->value()/0.4536)*233.4)/1000)+((((float)ui->spinCargoD->value()/0.4536)*287.6)/1000)+((((float)ui->spinBg4->value()/0.4536)*294.5)/1000)+((((float)ui->spinBg6->value()/0.4536)*344)/1000)+((float)ui->spinTks->value()*9.2*195.77/1000)-7.11)/(Gvzl/0.4536))*1000);
        float Centr = (CG-177.57)/0.664;
        ui->lineCentr->setText(QString::number((floor(Centr*100+.5)/100)));
        if (Centr>40.33) {
            ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
            QMessageBox msgBox;
            QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
            msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
            msgBox.setText(tr("Предельно задняя центровка!! "));
            msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
            okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
            msgBox.exec();
        }

        if(Gvzl<=3629){
            float CentrMin = ((Gvzl - 2327.6885245902)/54.6769527483);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        } else if((Gvzl>3629)and (Gvzl<=3969)){
            float CentrMin = ((Gvzl - 2698.8850574713)/39.0804597701);
            if (Centr < CentrMin){
                ui->lineCentr->setStyleSheet("color: red; background-color: rgb(255, 255, 255);");
                QMessageBox msgBox;
                QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
                msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
                msgBox.setText(tr("Предельно предняя центровка!! ").append(QVariant(CentrMin).toString()));
                msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
                okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
                msgBox.exec();
            }
        }

        ui->lineCG->setText(QString::number(round(CG)));//вывод плеча CG location(для графика)
        //Предельная загрузка
        float Gpred = 3969 - (Gpust + (float)Gfuel + (float)Gsl + ((float)ui->spinEk->value()*vEk) + ((float)ui->spinTks->value()*4.1731) -(float)16);
        ui->linePr->setText(QString::number(round(Gpred)));//вывод предельной загрузки

    }
}

void MainWindow::lbs()//Фунты->Кг
{
    int kg1;
    int lbs1 = ui->lineLbs1->text().toInt();
    ui->lineKg1->clear();
    kg1 =  round(lbs1*0.45359237);
    ui->lineKg1->setText(QString::number(kg1));
    ui->lineKg2->clear();
    ui->lineLbs2->clear();
}

void MainWindow::kg()//Кг->Фунты
{
    int lbs2;
    int kg2 = ui->lineKg2->text().toInt();
    ui->lineLbs2->clear();
    lbs2 = round(kg2*2.2046223302272);
    ui->lineLbs2->setText(QString::number(lbs2));
    ui->lineKg1->clear();
    ui->lineLbs1->clear();
}

void MainWindow::mm()//мм-ГПа
{
    int gp;
    int mm = ui->lineMM->text().toInt();
    ui->lineGP->clear();
    gp = round(mm*1.33322);
    ui->lineGP->setText(QString::number(gp));
    ui->lineQNH->setText(QString::number(gp));

}

void MainWindow::ha()//давление аэродрома по высоте и приведенному давлению
{
    int qfear;
    int har = ui->lineHA->text().toInt();
    int qnhar = ui->lineQNH->text().toInt();
    ui->lineQFE->clear();
    qfear = round(qnhar-(har/8));
    ui->lineQFE->setText(QString::number(qfear));
}

void MainWindow::vet()//вычисление боковой составляющей
{
    float ub;
    ui->lineUB->clear();
    float u = ui->lineU->text().toFloat();
    float uv = ui->lineUV->text().toFloat();
    ub = (sin(uv* 3.14159265 / 180))*u;
    ui->lineUB->setText(QString::number((floor(ub*10+.5)/10)));
}

void MainWindow::mettaf()//запрос metar taf
{
    QMessageBox msgBox;
    QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
    msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
    msgBox.setText(tr("Тестовая функция! :)"));
    msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
    okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
    msgBox.exec();
}

void MainWindow::mc()//перевод м/с в км/ч
{
    ui->lineKM->clear();
    float vmc = ui->lineMC->text().toFloat();
    float vkm = ((vmc*3600)/1000);
    ui->lineKM->setText(QString::number(round(vkm)));//вывод км/ч
}

void MainWindow::about()
{
    QMessageBox msgBox;
    QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
    msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
    msgBox.setText(tr("Расчёты для Cessna 208B \n ООО Аэрогео \n Версия 2.2.1\n Автор: Зуев М.Г.\n pilot40@gmail.com \n Лицензия: GPL v.3"));
    msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
    okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
    msgBox.exec();
}

void MainWindow::help()
{
    QMessageBox msgBox;
    QPushButton *okButton = msgBox.addButton(QMessageBox::Ok);
    msgBox.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.488, y1:0, x2:0.497, y2:1, stop:0 rgba(210, 216, 223, 255), stop:1 rgba(146, 165, 186, 255));font: 12pt ;Arial;font: bold;");
    msgBox.setText(tr("П-пассажирский вариант \n С1-с/з одни носилки \n С2-с/з двое носилок\n SP - больной\n ПЕР - перегон\n РАБ - работа\n Угол ветра + справа - слева"));
    msgBox.setWindowIcon(QIcon(QDir::currentPath() + "/67430.ico"));
    okButton->setStyleSheet("background-color: rgb(255, 255, 255);");
    msgBox.exec();
}


void MainWindow::closeapp()
{
    close();
}

void MainWindow::showExpanded()
{
#if defined(Q_OS_SYMBIAN) || defined(Q_WS_SIMULATOR)
    showFullScreen();
#elif defined(Q_WS_MAEMO_5)
    showMaximized();
#else
    show();
#endif
}
